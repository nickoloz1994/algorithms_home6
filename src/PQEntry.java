import java.util.Objects;

public class PQEntry<K,V> implements Entry<K,V>{
    private K k;
    private V v;

    public PQEntry(K key, V value){
        k = key;
        v = value;
    }

    @Override
    public K getKey() {
        return k;
    }

    @Override
    public V getValue() {
        return v;
    }

    protected void setKey(K key){
        k = key;
    }

    protected void setValue(V value){
        v = value;
    }

    @Override
    public boolean equals(Object o){
        if(o == null){
            return false;
        }

        PQEntry<K,V> e = (PQEntry<K,V>)o;
        if(this.getKey().equals(e.getKey()) && this.getValue().equals(e.getValue())){
            return true;
        }

        return false;
    }

    @Override
    public int hashCode(){
        return Objects.hash(k, v);
    }
}
