import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph example1 = new Graph("Example 1");
        example1.createRandomSimpleGraph(500, 15000);
        example1.printGraphInfo();
        example1.mstFromRandomVertex();

        Graph example2 = new Graph("Example 2");
        example2.createRandomSimpleGraph(20, 100);
        example2.printGraphInfo();
        example2.mstFromRandomVertex();

        Graph example3 = new Graph("Example 3");
        example3.createRandomSimpleGraph(0, 0);
        example3.printGraphInfo();
        example3.mstFromRandomVertex();

        Graph example4 = new Graph("Example 4");
        example4.createRandomSimpleGraph(2, 1);
        example4.printGraphInfo();
        example4.mstFromRandomVertex();

        Graph g = new Graph("G");
        g.createRandomSimpleGraph(6, 9);
        g.printGraphInfo();
        Scanner reader = new Scanner(System.in);
        List<String> vertices = g.getVertexIds();
        String v = "";
        while(!vertices.contains(v.toLowerCase())){
            System.out.println("Please enter valid vertex ID");
            v = reader.nextLine();
        }
        reader.close();
        System.out.println("Minimum Spanning Tree:");
        g.primJarnik(v);
    }

    /**
     * <code>Vertex</code> represents one vertex of the graph
     */
    class Vertex implements Iterator<Vertex> {

        private String id;
        private Vertex next;
        private Arc first;
        private int label;
        private List<Arc> edges;
        private int info = 0;

        /**
         * Class constructor specifying name of the vertex,
         * next vertex, first arc and value of vertex label
         *
         * @param s name of the vertex
         * @param v next vertex of the graph
         * @param e first edge of the vertex
         * @param l label of the vertex
         */
        Vertex(String s, Vertex v, Arc e, int l) {
            id = s;
            next = v;
            first = e;
            label = l;
            edges = new ArrayList<>();
        }

        /**
         * Class constructor specifying name of the vertex
         *
         * @param s name of the vertex
         */
        Vertex(String s) {
            this(s, null, null, 0);
        }

        /**
         * Class constructor
         */
        Vertex() {
        }

        /**
         * {@inheritDoc}
         *
         * @return
         */
        @Override
        public String toString() {
            return id;
        }

        /**
         * Sets label value of the vertex
         *
         * @param l integer value of the label
         */
        public void setLabel(int l) {
            label = l;
        }

        /**
         * Gets label value of the vertex
         *
         * @return integer value of the label
         */
        public int getLabel() {
            return label;
        }

        /**
         * Adds arc to the list of incident arcs of v
         *
         * @param a arc to be inserted in the list of incident arcs to vertex
         */
        public void addArc(Arc a) {
            edges.add(a);
        }

        /**
         * {@inheritDoc}
         *
         * @return
         */
        @Override
        public boolean hasNext() {
            return (next() != null);
        }

        /**
         * {@inheritDoc}
         *
         * @return
         */
        @Override
        public Vertex next() {
            return next;
        }
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc implements Iterator<Arc> {

        private String id;
        private Vertex source;
        private Vertex target;
        private Arc next;
        int weight;
        private int info = 0;

        /**
         * Class constructor specifying name of the arc, source vertex,
         * target vertex and weight of the arc
         *
         * @param s name of the arc
         * @param v source vertex
         * @param a target vertex
         * @param w weight of the arc
         */
        Arc(String s, Vertex v, Arc a, int w) {
            id = s;
            target = v;
            next = a;
            weight = w;
        }

        /**
         * Class constructor specifying name of the arc
         *
         * @param s name of the arc
         */
        Arc(String s) {
            this(s, null, null, 0);
        }

        /**
         * Sets the weight of the arc
         *
         * @param w value of the weight
         */
        public void setWeight(int w) {
            weight = w;
        }

        /**
         * Gets the weight of the arc
         *
         * @return weight of the arc
         */
        public int getWeight() {
            return weight;
        }

        public void setSource(Vertex v) {
            source = v;
        }

        /**
         * {@inheritDoc}
         *
         * @return
         */
        @Override
        public String toString() {
            return id;
        }

        /**
         * {@inheritDoc}
         *
         * @return
         */
        @Override
        public boolean hasNext() {
            return (next() != null);
        }

        /**
         * {@inheritDoc}
         *
         * @return
         */
        @Override
        public Arc next() {
            return next;
        }
    }


    /**
     * <code>Graph</code> represents undirected, wieghted graph
     */
    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;
        // You can add more fields, if needed

        /**
         * Class constructor specifying the name of the graph
         * and a first vertex of the graph
         *
         * @param s name of the graph
         * @param v first vertex of the graph
         */
        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        /**
         * Class constructor specifying the name of the graph
         *
         * @param s name of the graph
         */
        Graph(String s) {
            this(s, null);
        }

        /**
         * {@inheritDoc}
         *
         * @return
         */
        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        /**
         * Creates new vertex in the graph
         *
         * @param vid name of the vertex
         * @return newly created vertex
         */
        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        /**
         * Creates new arc in the graph from source to target vertex
         *
         * @param aid    arc name
         * @param from   source vertex
         * @param to     target vertex
         * @param weight weight of the arc
         * @return newly created arc
         */
        public Arc createArc(String aid, Vertex from, Vertex to, int weight) {
            Arc res = new Arc(aid);
            res.setWeight(weight);
            res.setSource(from);
            from.addArc(res);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            Random rn = new Random();
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                int w = rn.nextInt(10) + 1;
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i], w);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr], w);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Print out adjacency matrix of the graph
         *
         * @param matrix adjacency matrix to be printed out
         */
        public void printMatrix(int[][] matrix) {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    System.out.print(matrix[i][j] + " ");
                }
                System.out.println();
            }
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            Random rn = new Random();
            while (edgeCount > 0) {
                int w = rn.nextInt(10) + 1;
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj, w);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi, w);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Lists all vertices of the graph
         *
         * @return list of graph vertices
         */
        public List<Vertex> vertices() {
            List<Vertex> vertices = new ArrayList<>();
            Vertex v = first;
            while (v != null) {
                vertices.add(v);
                v = v.next();
            }

            return vertices;
        }

        /**
         * Returns list of vertex IDs
         *
         * @return list of vertex IDs
         */
        public List<String> getVertexIds() {
            List<String> ids = new ArrayList<>();
            for (Vertex v : this.vertices()) {
                ids.add(v.id);
            }

            return ids;
        }

        /**
         * Lists all arcs of the graph
         *
         * @return list of graph arcs
         */
        public List<Arc> arcs() {
            List<Arc> arcs = new ArrayList<>();
            List<Vertex> vertices = this.vertices();
            for (Vertex v : vertices) {
                Arc a = v.first;
                while (a != null) {
                    arcs.add(a);
                    a = a.next();
                }
            }

            return arcs;
        }

        /**
         * Finds the minimum spanning tree of the graph
         * given a starting vertex
         *
         * @param vertex starting point for finding MST
         * @return minimum spanning tree of the graph
         */
        public void primJarnik(String vertex) {
            List<Vertex> vertices = vertices();
            HashMap<Vertex, Arc> mstEdges = new HashMap<>();
            PriorityQueue<Integer, Vertex> Q = new HeapPriorityQueue<>();
            List<Arc> mstArcs = new ArrayList<>();
            List<Vertex> mst = new ArrayList<>();
            List<Node> nodes = new ArrayList<>();
            Vertex s;

            if (vertices.size() == 0) {
                throw new RuntimeException("Graph doesn't contain any vertices. Prim-Jarnik algorithm can not be run on empty graph");
            }

            for (Vertex ver : vertices) {
                if (ver.id.equals(vertex.toLowerCase())) {
                    s = ver;
                    s.setLabel(0);
                } else {
                    ver.setLabel(Integer.MAX_VALUE);
                }
            }

            for (Vertex ver : vertices) {
                Q.insert(ver.getLabel(), ver);
            }

            while (!Q.isEmpty()) {
                Vertex u = Q.removeMin().getValue();

                if (mstEdges.containsKey(u)) {
                    mstArcs.add(mstEdges.get(u));
                }

                for (Arc a : u.edges) {
                    Entry<Integer, Vertex> entry = new PQEntry<>(a.target.getLabel(), a.target);

                    if (Q.contains(entry)) {
                        if (a.getWeight() < entry.getKey()) {
                            Q.remove(entry);
                            Vertex target = entry.getValue();
                            target.setLabel(a.getWeight());
                            mstEdges.put(target, a);
                            Q.insert(target.getLabel(), target);
                        }
                    }
                }
            }

            constructMst(mst, nodes, mstArcs);
        }

        private void constructMst(List<Vertex> mst, List<Node> nodes, List<Arc> mstArcs) {
            int mstSum = 0;
            for (Arc a : mstArcs) {
                Node src = new Node();
                Node target = new Node();
                if (!mst.contains(a.source)) {
                    mst.add(a.source);
                    src.setName(a.source.id);
                    target.setName(a.target.id);
                    nodes.add(src);
                } else {
                    for (Node n : nodes) {
                        if (n.getName() == a.source.id) {
                            src = n;
                            target.setName(a.target.id);
                            break;
                        }
                    }
                }

                if (src.getFirstChild() == null) {
                    src.setFirstChild(target);
                } else {
                    Node ch = src.getFirstChild();
                    while (ch.next() != null) {
                        ch = ch.next();
                    }
                    ch.setNextSibling(target);
                }
            }

            for (Node n : nodes) {
                System.out.print(n.getName());
                if (n.getFirstChild() != null) {
                    Node c = n.getFirstChild();
                    System.out.print(" --> " + c.getName());
                    while (c.next() != null) {
                        System.out.printf(" %s --> %s ", n.getName(), c.next().getName());
                        c = c.next();
                    }
                }

                System.out.println();
            }

            for(Arc a : mstArcs){
                mstSum += a.getWeight();
            }

            System.out.printf("The weight of minimum spanning tree is %d \n", mstSum);
        }

        private void printGraphInfo() {
            List<Vertex> vertices = vertices();
            List<Arc> arcs = arcs();
            System.out.println(id);
            if (vertices.size() == 0) {
                System.out.println("Given graph doesn't contain any vertices, no information can be displayed for an empty graph");
                return;
            }

            System.out.println("Original graph:");
            System.out.println(this);
            System.out.println("Adjacency matrix of the original graph;");
            this.printMatrix(this.createAdjMatrix());

            System.out.println("Arcs of the graph with respective weights:");
            for(int i = 0; i < arcs.size(); i++){
                if(i%10 == 0){
                    System.out.println();
                }

                System.out.printf("Arc %s(%d) | ", arcs.get(i).id, arcs.get(i).getWeight());
            }

            System.out.println();
            System.out.println("Vertices of the graph:");
            for(int i = 0; i < vertices.size(); i++){
                if(i%40 == 0){
                    System.out.println();
                }

                System.out.printf("%s ", vertices.get(i).id);
            }
            System.out.println();
        }

        private void mstFromRandomVertex() {
            List<Vertex> vertices = vertices();
            if (vertices.size() == 0) {
                System.out.println("Given graph doesn't contain any vertices, Minimum spanning tree can not be constructed for empty graph");
                System.out.println("=============================================================");
                return;
            }

            int index = (int) (Math.random() * vertices.size());
            Vertex start = vertices.get(index);

            System.out.println("Minimum spanning tree:");
            primJarnik(start.id);

            System.out.println("=============================================================");
        }
    }
}