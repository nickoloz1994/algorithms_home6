import java.util.*;

public class Node implements Iterator<Node> {

    private String name;
    private Node firstChild;
    private Node nextSibling;
    private int info;

    Node(){}

    Node(String n, Node d, Node r) {
        // TODO!!! Your constructor here
        setName(n);
        setFirstChild(d);
        setNextSibling(r);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setFirstChild(Node child) {
        this.firstChild = child;
    }

    public Node getFirstChild() {
        return this.firstChild;
    }

    public void setNextSibling(Node sibling) {
        this.nextSibling = sibling;
    }

    public int getInfo() {
        return this.info;
    }

    public void setInfo(int i) {
        this.info = i;
    }

    public boolean hasChild() {
        return (getFirstChild() != null);
    }

    @Override
    public boolean hasNext() {
        return (this.nextSibling != null);
    }

    @Override
    public Node next() {
        return this.nextSibling;
    }
}
