// Reference: Data Structures and Algorithms in Java (6th Edition) Page 362
public interface Entry<K,V> {
    K getKey();
    V getValue();
}
